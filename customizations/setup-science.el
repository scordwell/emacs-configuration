;;; setup-science --- Customisations to work with scientfic software
;;; Commentary:

;;; Code:
(require 'ess-site)
(add-to-list 'ess-tracebug-search-path "~/usr/src/julia/base/")

(require 'poly-markdown)
(add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))

(provide 'setup-science)
;;; setup-science.el ends here
