;;; setup-git --- Customisations to to use Git
;;; Commentary:

;;; Code:
(require 'magit)
(global-set-key "\C-xg" 'magit-status)

(global-set-key [f7] 'find-file-in-repository)

(provide 'setup-git)
;;; setup-git.el ends here
