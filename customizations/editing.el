;;; editing --- Customise buffer editing
;;; Commentary:
;; These customizations make editing a bit nicer.

;;; Code:
;; Tell Emacs where to read abbrev definitions from
(setq abbrev-file-name
      "~/.emacs.d/abbrev_defs")
;; Save abbrevs when files are saved
(setq save-abbrevs t)
;; Turn on abbrev-mode globally
(setq-default abbrev-mode t)

;; Key binding to use "hippie expand" for text autocompletion
;; http://www.emacswiki.org/emacs/HippieExpand
(global-set-key (kbd "M-/") 'hippie-expand)

;; Lisp-friendly hippie expand
(setq hippie-expand-try-functions-list
      '(try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill
        try-complete-lisp-symbol-partially
        try-complete-lisp-symbol))

;; Highlights matching parenthesis
(show-paren-mode 1)

;; Highlight current line
(global-hl-line-mode 1)

;; Interactive search key bindings. By default, C-s runs
;; isearch-forward, so this swaps the bindings.
(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)

;; Don't use hard tabs
(setq-default indent-tabs-mode nil)

;; When you visit a file, point goes to the last place where it
;; was when you previously visited the same file.
;; http://www.emacswiki.org/emacs/SavePlace
(require 'saveplace)
(setq-default save-place t)
;; keep track of saved places in ~/.emacs.d/places
(setq save-place-file (concat user-emacs-directory "places"))

;; Emacs can automatically create backup files. This tells Emacs to
;; put all backups in ~/.emacs.d/backups. More info:
;; http://www.gnu.org/software/emacs/manual/html_node/elisp/Backup-Files.html
(setq backup-directory-alist `(("." . ,(concat user-emacs-directory
                                               "backups"))))
(setq auto-save-default nil)

;; Comments
(defun toggle-comment-on-line ()
  "Comment or uncomment current line."
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))
(global-set-key (kbd "C-;") 'toggle-comment-on-line)

;; yay rainbows!
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
;(global-rainbow-delimiters-mode t)

;; fix weird os x kill error
; (defun ns-get-pasteboard ()
;  "Returns the value of the pasteboard, or nil for unsupported formats."
;  (condition-case nil
;      (ns-get-selection-internal 'CLIPBOARD)
;    (quit nil)))


(require 'auto-complete)
;; auto-complete settings
(setq
 ac-auto-start 2
 ac-override-local-map nil
 ac-use-menu-map t
 ac-candidate-limit 20)

(require 'autopair)

(require 'yasnippet)
(yas-global-mode 1)

(require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)

(provide 'editing)
;;; editing.el ends here
