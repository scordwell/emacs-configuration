;;; init --- My Emacs Configuration
;;; Commentary:
;; These are my customisations.

;;; Code:

;; Define package repositories
(require 'package)

;;; Code:
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
;; (add-to-list 'package-archives
;; 	     '("melpa stable" . "http://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("org" . "http://orgmode.org/elpa/") t)


;; Load and activate emacs packages. Do this first so that the
;; packages are loaded before you start trying to modify them.
;; This also sets the load path.
(package-initialize)


;;; Bootstrap use-package
;; Install use-package if it's not already installed.
;; use-package is used to configure the rest of the packages.
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; From use-package README
(eval-when-compile
  (require 'use-package))
(require 'diminish)
(require 'bind-key)

(setq use-package-always-ensure t)

;; Download the ELPA archive description if needed.
;; This informs Emacs about the latest versions of all packages, and
;; makes them available for download.
(when (not package-archive-contents)
  (package-refresh-contents))

;; The packages you want installed. You can also install these
;; manually with M-x package-install
;; Add in your own as you wish:
(defvar my-packages
  '(;; makes handling lisp expressions much, much easier
    ;; Cheatsheet: http://www.emacswiki.org/emacs/PareditCheatsheet
    paredit

    autopair

    flycheck

    ;; allow ido usage in as many contexts as possible. see
    ;; customizations/better-defaults.el line 47 for a description
    ;; of ido
    ido-ubiquitous

    ;; Python analysis
    jedi

    ;; Enhances M-x to allow easier execution of commands. Provides
    ;; a filterable list of possible commands in the minibuffer
    ;; http://www.emacswiki.org/emacs/Smex
    smex

    ;; project navigation
    projectile

    ;; colorful parenthesis matching
    rainbow-delimiters

    ;; edit html tags like sexps
    tagedit

    ;; git integration
    magit

    ;; http://www.emacswiki.org/emacs/Yasnippet
    yasnippet

    zenburn-theme

    ;; Major modes

    ess

    haskell-mode

    markdown-mode

    python-mode

    polymode))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

;;;;
;; Customization
;;;;

;; https://www.youtube.com/watch?v=kt-wLpz-9PA
(defadvice transpose-words
    (before transpose-last-word)
  "Transpose last two words when at end of line."
  (when (looking-at "$")
    (backward-word 1)))

(ad-activate 'transpose-words)

;; Show column numbers
(setq column-number-mode t)

(global-visual-line-mode 1)
(mouse-avoidance-mode 'banish)

;; Font Settings
(defun sc/get-fixed-font-family ()
  "Get the fixed font family for the OS in use."
  (if (eq system-type 'windows-nt)
      "Consolas"
    "Source Code Pro"))

(defvar sc/fixed-font-family
  (sc/get-fixed-font-family))
(set-frame-font sc/fixed-font-family)
(set-face-attribute 'default nil :font sc/fixed-font-family :height 110)
(set-face-font 'default sc/fixed-font-family)

;; Add a directory to our load path so that when you `load` things
;; below, Emacs knows where to look for the corresponding file.
(add-to-list 'load-path "~/.emacs.d/customizations")

(load "navigation.el")

(load "ui.el")

(load "editing.el")

(load "misc.el")

(load "elisp-editing.el")

(load "setup-git.el")

(load "setup-science.el")

(load "setup-org.el")

(load "setup-haskell.el")

(load "setup-python.el")

;; Keys
(global-set-key "\C-xc" 'calendar)
(global-set-key "\C-xt" 'eshell)
(global-set-key (kbd "<f12>") 'menu-bar-mode)

;; ssh
(require 'tramp)
(setq tramp-default-method "ssh")

;; Server
(server-start)

(provide 'init)
;;; init.el ends here
